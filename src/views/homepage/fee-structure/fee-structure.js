import React from "react";
import { Container, Row, Col, Image } from "react-bootstrap";
import classnames from "classnames";
import { PricingBox } from "../../../common/pricing-box/pricing-box";
import SectionHeading from "../../../common/section-heading/section-heading";

import tickPointer from "../../../assets/images/common/tick.png";
const headingText = "Waterfall Fee Structure";
const trailTooltip = "Fee Structure Example: An estimate grand total of $100k will result in a fee of 7.36% ($7,360). The $750 deposit will be collected upon submission and applied toward the overarching fee.";
const enterpriseTooltip = "Fee Structure Example: An estimate grand total of $100k will result in a 4.86% fee ($4,860). The $750 deposit will be collected upon submission and applied toward the overarching fee.";

const PricingPlanBenefits = (props) => {
    const { title, price, benefits } = props?.data;
    const Benefits = benefits?.map((value, index) => {
        return (
            <React.Fragment key={index}>
                <div key={index} className={classnames('d-flex align-items-center mb-1')}>
                    <div>
                        <Image src={tickPointer} className={classnames('mx-2')} alt="tick" width="14px" height="14px" />
                    </div>
                    <div>{value}</div>
                </div>
            </React.Fragment>


        );
    })
    return (
        <React.Fragment>
            <div className={classnames('card-title card-bold-text')}>{title}</div>
            <React.Fragment>{Benefits}</React.Fragment>
            <div className={classnames('card-bold-text')}><p>{price}</p></div>
        </React.Fragment>
    )
}

class WaterfallFeeStructure extends React.Component {

    state = {
        trail_plan: { title: '7.50%', price: 'Free', benefits: ['Access to mpartial Scope', 'Minimum $750 initial deposit'] },
        enterprise_plan: {
            title: '4.95%', price: '$495/month', benefits: [
                'Access to mpartial Scope',
                'Discounted fee schedule',
                'Unlimited collaborators',
                'Dedicated account manager',
                'Prioritized turnaround time',
                'Minimum $750 initial deposit',
            ]
        }
    }

    render() {
        const { trail_plan, enterprise_plan } = this.state;
        return (
            <div>
                <SectionHeading position="center" headingText={headingText}></SectionHeading>
                <Container>
                    <p className={classnames('section_title_description')}>Drag the slider around based on what you think it will cost to repair the property. <br /> Move forward based on the estimated fee structure below.</p>
                    <div className={classnames('mt-5 pt-5 mb-5')}>
                        <Row>
                            <Col md={{ span: 5, offset: 1 }} sm="12">
                                <PricingBox color="primary" title="Trial Plan" tooltipData={trailTooltip}>
                                    <PricingPlanBenefits data={trail_plan} />
                                </PricingBox>
                            </Col>
                            <Col md="5" sm="12">
                                <PricingBox color="secondary" title="Enterprise Plan" tooltipData={enterpriseTooltip}>
                                    <PricingPlanBenefits data={enterprise_plan} />
                                </PricingBox>
                            </Col>
                        </Row>
                    </div>
                </Container>
            </div>
        )
    }
}

export default WaterfallFeeStructure