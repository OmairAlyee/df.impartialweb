import React from "react";
import { Container, Row, Col, Image } from "react-bootstrap";
import classnames from "classnames";
import SectionHeading from "../../../common/section-heading/section-heading";
import SlickSlider from "../../../common/slick-slider/slick-slider";
import image0 from "../../../assets/images/grey-image/image0.png";
import image1 from "../../../assets/images/grey-image/image1.png";
const headingText = "Example Deliverables";

class ExampleDeliverables extends React.Component {

    render() {
        return (
            <div className={classnames('bg-primary')}>
                <SectionHeading position="center" headingText={headingText}></SectionHeading>
                <Container>
                    <section>
                        <h3 className={classnames('font-size-3 text-center')}>Pre-Mitigation Scan + Post-Mitigation Scan = ESX & SKX (TruePlan)</h3>
                        <Row>
                            <Col>
                                <section className={classnames('my-3')}>
                                    <iframe src="https://my.matterport.com/show/?m=e5Wxtu8Arbx" title="pre" width="100%" height="480" allowFullScreen />
                                </section>
                            </Col>
                            <Col>
                                <section className={classnames('my-3')}>
                                    <iframe src="https://my.matterport.com/show/?m=DhqzGgT7M1E" title="post" width="100%" height="480" allowFullScreen />
                                </section>
                            </Col>
                        </Row>
                    </section>
                    <section className={classnames('my-3')}>
                        <h3 className={classnames('font-size-3 text-center')}>Immaculate. Impartial. [ESX]</h3>
                        <SlickSlider />
                    </section>
                    <section className={classnames('my-3')}>
                        <h3 className={classnames('font-size-3 text-center')}>MATTERPORT TRUEPLAN™ FOR XACTIMATE™</h3>
                        <Row className={classnames('sketch-container py-5')}>
                            <Image src={image0} alt="image grey 1" className={classnames('image-sketch')} width="100%" />
                            <Image src={image1} alt="image grey 1" className={classnames('image-sketch')} width="100%" />
                        </Row>
                    </section>
                </Container>
            </div>
        )
    }
}

export default ExampleDeliverables