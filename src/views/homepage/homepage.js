import React from "react";
import SlickSlider from "../../common/slick-slider/slick-slider";
import TruthDataTrusts from "./truth-data-trusts/truth-data-trusts";
import HowItWorks from "./how-it-works/how-it-works";
import WaterfallFeeStructure from "./fee-structure/fee-structure";
import ExampleDeliverables from "./deliverables/deliverables";
import ContactUs from "./contact-us/contact-us";

class Homepage extends React.Component {

    render() {
        return (
            <div>
                <SlickSlider isVideo={true} />
                <TruthDataTrusts />
                <HowItWorks />
                <WaterfallFeeStructure />
                <ExampleDeliverables />
                <ContactUs />
            </div>
        )
    }
}

export default Homepage