import React from "react";
import { Button } from "react-bootstrap";
import classnames from "classnames";
import SectionHeading from "../../../common/section-heading/section-heading";
import ReactImageSlider from "../../../common/react-image-slider/react-image-slider";

import preMitigation from "../../../assets/images/common/pre-mitigation.jpg";
import postMitigation from "../../../assets/images/common/post-mitigation.jpg";
const description = "We have combined the best-of-breed technology platforms with an eye towards relieving you of administrative burden. Matterport 3D scans augmented by TruePlan are used to generate consistent Xactimate sheets that are delivered in accord with Actionable Insights compliance rule sets. Each mpartial is produced with full transparency, unprecedented forensic photography, and infallible geospatial data that collectively result in rapid approvals.";
const headingText = "GROUND-TRUTH DATA EVERYONE TRUSTS";

class TruthDataTrusts extends React.Component {

    render() {
        return (
            <div>
                <SectionHeading position="left" headingText={headingText} description={description}></SectionHeading>
                <div className={classnames('compare_images_section')}>
                    <ReactImageSlider imageOne={preMitigation} imageTwo={postMitigation} />
                </div>
                <div className={classnames('try_now_btn')}>
                    <Button className={classnames('btn btn-success rounded')}>Try Today</Button>
                </div>
            </div>
        )
    }
}

export default TruthDataTrusts