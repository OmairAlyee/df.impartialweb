import React from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import classnames from "classnames";
import SectionHeading from "../../../common/section-heading/section-heading";
import ReactGoogleRecaptcha from "../../../common/react-google-recaptcha/react-google-recaptcha";
const headingText = "Contact Us";

class ContactUs extends React.Component {
    verifyCallbackAuth(response) {
        if (response !== undefined && typeof response === 'string' && response?.length > 0)
            this.setState({ isRecaptchaVerified: true });
    }
    expiredCallbackAuth() {
        this.setState({ isRecaptchaVerified: false });
        console.log("%c reCaptcha is Expired", 'color: #800000')
    }

    render() {
        return (
            <div>
                <SectionHeading position="center" headingText={headingText}></SectionHeading>
                <Container>
                    <Form>
                        <Row className={classnames('form-holder form-group')}>
                            <Col md="6" sm="12">
                                <div className={classnames('form-group')}>
                                    <input type="text" className={classnames('form-control')} placeholder="Your Name" />
                                </div>
                                <div className={classnames('form-group')}>
                                    <input type="email" className={classnames('form-control')} placeholder="Email" />
                                </div>
                                <div className={classnames('form-group')}>
                                    <input type="text" className={classnames('form-control')} placeholder="Cell" />
                                </div>
                            </Col>
                            <Col md="6" sm="12">
                                <div className={classnames('form-group')}>
                                    <textarea className={classnames('form-control')} placeholder="Write your message..."></textarea>
                                </div>
                            </Col>
                        </Row>
                    </Form>
                    <Row className={classnames('justify-content-center form-group')}>
                    
                    <Col md="12" className={classnames('d-flex justify-content-center form-group')}>
                    <ReactGoogleRecaptcha onVerifyCallbackHandler={this.verifyCallbackAuth.bind(this)} onExpiredRecaptcha={this.expiredCallbackAuth.bind(this)} />        
                        </Col>
                        <Col>
                            <div className={classnames('try_now_btn widget widgetsubmit')}>
                                <Button className={classnames('btn btn-success rounded')}>Submit</Button>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

export default ContactUs