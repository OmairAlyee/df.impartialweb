import React from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import classnames from "classnames";
import SectionHeading from "../../../common/section-heading/section-heading";
import StepOneByOne from "../../../common/step-one-by-one/step-one-by-one";

const headingText = "How It Ground";

class HowItWorks extends React.Component {

    render() {
        return (
            <div className={classnames('bg-primary')}>
                <SectionHeading position="center" headingText={headingText}></SectionHeading>
                <Container>
                    <div className={classnames('pt-4')}>
                        <Row className={classnames('mx-0')}>
                            <Col md="4" sm="12" className={classnames('px-0')}>
                                <StepOneByOne step="1" description="Perform pre-mitigation and post-mitigation scans with a Matterport Pro Series camera." />
                            </Col>
                            <Col md="4" sm="12" className={classnames('px-0')}>
                                <StepOneByOne step="2" description="Submit the Matterport scans via the mpartial portal and then go back to what you do great." />
                            </Col>
                            <Col md="4" sm="12" className={classnames('px-0')}>
                                <StepOneByOne step="3" description="Receive a well-formatted, fully documented Xactimate PDF, ESX & Matterport TruePlan SKX." />
                            </Col>
                        </Row>
                    </div>
                    <div className={classnames('try_now_btn widget')}>
                        <Button className={classnames('btn btn-success rounded')}>Get Started</Button>
                    </div>
                </Container>
            </div>
        )
    }
}

export default HowItWorks