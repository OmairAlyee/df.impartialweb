import React from "react";
import { Image } from "react-bootstrap";
import { ReactCompareSlider, ReactCompareSliderImage } from 'react-compare-slider';
import classnames from "classnames";
import sliderButton from "../../assets/images/common/react-slider-button.png";
import "./react-image-slider.scss";

const MyCustomHandle = () => (
    <div className={classnames('rcs-slider-line')}>
        <Image src={sliderButton} alt="slider-button-image" />
    </div>
);

class ReactImageSlider extends React.Component {

    render() {
        return (
            <ReactCompareSlider style={{ width: '100%' }}
                handle={<MyCustomHandle style={{ color: 'yellow' }} />}
                itemOne={<ReactCompareSliderImage src={this.props?.imageOne} srcSet={this.props?.imageOne} alt="Image one" />}
                itemTwo={<ReactCompareSliderImage src={this.props?.imageTwo} srcSet={this.props?.imageTwo} alt="Image two" />}
            />
        );
    }
}

export default ReactImageSlider