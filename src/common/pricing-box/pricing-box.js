import React from "react";
import { Row, Col, Card, OverlayTrigger, Tooltip } from "react-bootstrap";
import classnames from "classnames";

import "./pricing-box.scss";


export class PricingBox extends React.Component {

    render() {
        const { color, title, tooltipData } = this.props;
        return (
            <Card className={classnames('h-100')}>
                <Card.Header className={classnames({ "box-header-primary": color === 'primary', "box-header-secondary": color === 'secondary' })}>
                    <Row className={classnames('justify-content-center')}>
                        <Col>{title}</Col>
                        <Col className={classnames('position-relative')}>
                            <OverlayTrigger placement="top" overlay={<Tooltip id={`tooltip-top`}> {tooltipData} </Tooltip>}>
                                <i className={classnames('info_popup')}>i</i>
                            </OverlayTrigger>
                        </Col>
                    </Row>
                </Card.Header>
                <Card.Body className={classnames('m-auto')}>
                    {this.props?.children}
                </Card.Body>
            </Card>
        )
    }
}