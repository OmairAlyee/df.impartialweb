import React from "react";
import Recaptcha from 'react-recaptcha';
const RECAPTCHA_KEY_V2 = "6Le5UUkbAAAAAJocRzyUrmrt8z6RR6y20sgDxbnc";
const ReactGoogleRecaptcha = props => {
    const { onVerifyCallbackHandler, onExpiredRecaptcha } = props;
    
    // let recaptchaInstance;
    // create a reset function
    //  const letmeChanger = () => {
    //     recaptchaInstance.reset();
    // };

    var callback = function () {
        console.log('%c Google Recaptcha Loaded Successfully', 'color: #164d2f');
    };
    if (RECAPTCHA_KEY_V2?.length > 0) {
        console.log("Heloo")
        return (
            <Recaptcha
                ref={e => e}
                sitekey={RECAPTCHA_KEY_V2}
                size="normal"
                onloadCallback={callback}
                verifyCallback={onVerifyCallbackHandler}
                expiredCallback={onExpiredRecaptcha}
                render="explicit"
                tabindex={3}
                elementID="google-login-recaptcha"
            />
        )

    }
    else {
        return null
    }

}

export default ReactGoogleRecaptcha