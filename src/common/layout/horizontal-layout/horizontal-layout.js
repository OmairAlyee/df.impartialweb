import React from "react";
import HeaderMenu from "../header-menu/header-menu";
import Footer from "../footer/footer";

class HorizontalLayout extends React.Component {

    render() {
        return (
            <div>
                <HeaderMenu />
                <div>{this.props.children}</div>
                <Footer />
            </div>
        )
    }
}

export default HorizontalLayout