import React from "react";
import HorizontalLayout from "./horizontal-layout/horizontal-layout";

export const ContextLayout = React.createContext();

export class Layout extends React.Component {

    render() {
        const { children } = this.props;
        return (
            <ContextLayout.Provider value={{ layout: HorizontalLayout }} >
                {children}
            </ContextLayout.Provider>
        )
    }
}