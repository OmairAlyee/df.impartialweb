import React from "react";
import Spinner from "react-bootstrap/Spinner";

class SpinnerComponent extends React.Component {

    render() {
        return (
            <Spinner animation="border" variant="success" />
        )
    }
}

export default SpinnerComponent