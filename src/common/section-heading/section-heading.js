import classnames from "classnames";
import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import circleImage from "../../assets/images/common/circle-image.png";
import "./section-heading.scss";

class SectionHeading extends React.Component {

    state = {
        position: 'center',
        headingText: '',
        description: ''
    }

    componentDidMount() {
        this.setState({ position: this.props?.position, headingText: this.props?.headingText, description: this.props?.description });
    }

    render() {
        const { position, headingText, description } = this?.state;

        return (
            <Container>
                <div className={classnames(`title ${position}`)}>
                    <Row>
                        <Col className={classnames({ 'col-md-6 col-sm-12': position === 'left' || position === 'right' })}>
                            <h2 className={classnames('section_title_text')} style={{ backgroundImage: `url(${circleImage})` }}>{headingText}</h2>
                        </Col>
                        {description?.length > 0 ?
                            <Col md="6" sm="12">
                                <p className={classnames('section_title_description')}>{description}</p>
                            </Col>
                            : null
                        }
                    </Row>
                </div>
            </Container>
        )
    }
}

export default SectionHeading