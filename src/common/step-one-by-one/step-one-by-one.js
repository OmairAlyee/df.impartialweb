import React from "react";
import { Row, Col } from "react-bootstrap";
import classnames from "classnames";

import roundedCircle from "../../assets/images/common/rounded-circle.png";
import "./step-one-by-one.scss";

class StepOneByOne extends React.Component {

    render() {
        return (
            <Row className={classnames('mx-0 px-0')}>
                <Col md="4" sm="12" className={classnames('data_number px-0')} style={{ backgroundImage: `url(${roundedCircle})` }}>{this?.props?.step}</Col>
                <Col md="8" sm="12" className={classnames('data_text px-0')}>
                    <a href="/">{this?.props?.description}</a>
                </Col>
            </Row>
        )
    }
}

export default StepOneByOne