import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./slick-slider.scss";
import classnames from "classnames";
import { Image } from "react-bootstrap";
import Slider from "react-slick";
import { Carousel } from "react-bootstrap";
import videoMp4 from "../../assets/videos/homepage.mp4";
import image0 from "../../assets/images/reports/image0.jpg";
import image1 from "../../assets/images/reports/image1.jpg";
import image2 from "../../assets/images/reports/image2.jpg";
import image3 from "../../assets/images/reports/image3.jpg";


const SlickSlider = (props) => {
    const { isVideo } = props;
    if (isVideo) {
        return (
            <Slider infinite={false} autoPlay={true} muted>
                <video autoPlay={true} loop={true} muted>
                    <source src={videoMp4} type="video/mp4" />
                    <source src={videoMp4} type="video/ogg" />
                </video>
            </Slider>
        );
    } else {
        return (
            <div className={classnames('deliver-slider')}>
                <Carousel indicators={true} pause={false} interval={3000}>
                    <Carousel.Item>
                        <Image
                            className="d-block w-100"
                            src={image0}
                            alt="First slide"
                        />
                    </Carousel.Item>
                    <Carousel.Item>
                        <Image
                            className="d-block w-100"
                            src={image1}
                            alt="First slide"
                        />
                    </Carousel.Item>
                    <Carousel.Item>
                        <Image
                            className="d-block w-100"
                            src={image2}
                            alt="First slide"
                        />
                    </Carousel.Item>
                    <Carousel.Item>
                        <Image
                            className="d-block w-100"
                            src={image3}
                            alt="First slide"
                        />
                    </Carousel.Item>
                </Carousel>
            </div>
        );
    }

}

export default SlickSlider