import './assets/scss/style.scss';
import NavigationRoutes from "./core/routes/navigation-routes";

const App = (props) => {
  return (
    <NavigationRoutes />
  )
}

export default App;
