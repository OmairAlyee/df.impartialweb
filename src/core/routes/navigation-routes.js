import React, { Suspense, lazy } from "react"
import { Switch, Router, Route } from "react-router-dom";
import { history } from "../../history";
import { ContextLayout } from "../../common/layout/layout";
import Spinner from "../../common/spinner/spinner";
const Homepage = lazy(() => import("../../views/homepage/homepage"));

const RouteConfig = ({ component: Component, fullLayout, ...rest }) => (

    <Route {...rest} render={props => {
        return (
            <ContextLayout.Consumer>
                {(contexts) => {
                    let TagLayout = contexts?.layout
                    return (
                        <TagLayout>
                            <Suspense fallback={<Spinner />}>
                                <Component {...props} />
                            </Suspense>
                        </TagLayout>
                    )
                }}
            </ContextLayout.Consumer>

        )
    }}
    />
)
const AppRoute = (RouteConfig);

class NavigationRoutes extends React.Component {
    render() {
        return (
            <Router history={history}>
                <Switch>
                    <AppRoute exact path="/" component={Homepage} />
                </Switch>
            </Router>
        )
    }
}

export default NavigationRoutes