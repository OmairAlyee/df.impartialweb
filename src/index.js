import 'react-app-polyfill/ie9';
import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
import React, { Suspense, lazy } from "react";
import ReactDOM from 'react-dom';
import './index.scss';
import reportWebVitals from './reportWebVitals';
import Spinner from "./common/spinner/spinner";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Layout } from "./common/layout/layout";

const LazyApp = lazy(() => import("./App"))

ReactDOM.render(
  <React.StrictMode>
    <Suspense fallback={<Spinner />}>
      <Layout>
        <LazyApp />
      </Layout>

    </Suspense>

  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
